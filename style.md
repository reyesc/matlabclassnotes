# Style

## Syntax vs Style
__Syntax__ is a set of rigid, computer language-dependent rules that govern how 
programs must be written so that the computer can execute them. Without proper 
syntax, your program will not run.

__Style__ is how one expresses the syntax.

Correct syntax does not guarantee a correct program. Style makes it
easier to tell if the program is executing as intended by making the
intentions known

Consider the following program, which we will incrementally improve
through style.

### original program
```matlab
x=[3,4,5];x(4)=sqrt(x(1)^2)==sqrt(x(2)^2+x(3)^2);if x(4);disp('ja!');else;disp('nein!');end
```
The previous program is syntactically correct. It works. It is also _wrong_.  What is worse is that it is extremely hard to tell what was intended because it is such a visual mess.

### first improvement: Apply some basic formatting
__basic formatting__  
It is good practice to put each _statement_ on its own line. and to
to indent block of related statements.  The most common indentions are
for if/else/end blocksand for/end loops.

Spacing within each line is a matter of personal taste, but some spacings
are more readable than others

```matlab
% indentation example
for x = 1 : 5
    % indent this block to see how the FOR and END match.
    % everything between FOR and the matching END executes 5 times
    if x == 3
        % statements between IF and ELSE  execute only
        % during the 3rd loop, when x contains the value 3.
        disp('number 3')
    else
        % everything between ELSE and END will be performed when
        % the previous loop didn't
        disp('some other number')
    end
end

```
Back to our example, with improved formatting:
```matlab
x = [3,4,5];
x(4) = sqrt(x(1)^2) == sqrt( x(2)^2 + x(3)^2 );
if x(4)
    disp('ja!');
else
    disp('nein!');
end
```

> The MATLAB editor can automatically indent for you.  Select
the text to autoindent and choose (`command-i` or `ctrl-i`)

Now one can see broadly what the program is doing. 
1. Assign some values
2. compare a couple equations
3. display a message depending on the comparison

### second improvement: consider changing variable names
__variables names__  don't use a single variable for all your computations, unless it really makes sense.  In this case, we aren't manipulating x as a vector, and it appears that x is simply a convenient hold-all value. so, we can split
the various values out
```matlab
a = 3;
b = 4;
c = 5;

d = sqrt(a^2) == sqrt( b^2 + c^2 );

if d==true
    disp('ja!');
else
    disp('nein!');
end
```
At this point, it is much easier to tell what the math is trying to do.
It looks like it is comparing distances, maybe?  Short names like `a`, `b`, and `c` make sense in some mathematical contexts, so they're not
neccessarily "bad". In this case, however, `d` makes no sense.

better...

```matlab
...
isValid = sqrt(a^2) == sqrt( b^2 + c^2 );
if isValid
    ...
end
```
except isValid also doesn't tell us what the point of the comparison is.


### third improvement : Comment your code
__comments__ commenting code can help with the readability.  Comments are ignored by the computer, but can make a huge difference to anyone
trying to decipher the program.

First, an anti-example.  Comments should help with understanding WHY,
but the code itself should explain HOW.

#### poor commenting example
```matlab

% assign some parameters
a = 3; % assign 3 to a
b = 4; % assign 4 to b
c = 5; % assign 5 to c

isValid = sqrt(a^2) == sqrt( b^2 + c^2 ); % make sure the comparison is valid

if isValid
    % it is valid
    disp('ja!'); 
else
    % it isn't valid
    disp('nein!');
end
```
This merely restates what the program does. It offers no insight into why.

#### good commenting example
```matlab
% Test Pythagorean Theorem
a = 3; % beine 1
b = 4; % beine 2
c = 5; % hypoteneuse

isValid = sqrt(a^2) == sqrt( b^2 + c^2 ); % does pythag theorem work for this triangle?

if isValid
    disp('ja!'); 
else
    disp('nein!');
end
```
Now, we can see what the programmer intended to do. Also, we an see that the logic was incorrect.

furthermore...
since this is a triangle, then a, b, and c should all be positive.
This means that for any positive x, `sqrt(x^2) equals x` 
```matlab
isValid = c == sqrt( a^2 + b^2 ); % does pythag theorem work for this triangle?
```

### Fourth improvement : RENAMING VS COMMENTING
The program can be made even more readable by renaming certain variables _instead_ of commenting. Good variable names make it obvious what that
variable represents.  

```matlab
% Test Pythagorean Theorem
a = 3; % beine 1
b = 4; % beine 2
c = 5; % hypoteneuse

isRightTriangle = c == sqrt( a^2 + b^2 );

if isRightTriangle
    disp('ja!'); 
else
    disp('nein!');
end
```
personal preference dictates how far to take this...
```matlab
% Test Pythagorean Theorem
erste_bein = 3;
zweite_bein = 4;
hyptoeneuse = 5;

isRightTriangle = hypoteneuse == sqrt( erste_bein^2 + zweite_bein^2 );
% ... usw ...
```
* __note__ when choosing to rename variables, consider if it makes sense to include UNITS.  eg.  instead of simply `distance`, consider using `distance_km`, or `distance_m`, etc.  Likewise `degrees` could be `degrees_c`, `degrees_f`, or `latitude_degrees`
* __also note__ variable names can be arbitrarily long.  if you abbreviate names, be consistent.
* __also also note__ capitalization matters.  `deg` is different from `Deg` and `DEG`.

### BONUS: Reusing your code
when a section of code does one thing, and does it well, you might wish
to use it elsewhere. For example, if have a program dedicated to
detecting right-triangles, then I would likely package up that functionality into a __FUNCTION__.  Don't forget to describe the function with a comment right after the function declaration. This allows people to search for your function with `lookfor`, and provides
a guideline on how to use it.


```matlab
function TF = isRightTriangle(lengthA, lengthB, hypoteneuse)
    % isRightTriangle returns true if the triangle is a right triangle.

    TF = hypoteneuse^2 == lengthA^2 + lengthB^2;
end
```
The above should be saved in a file that matches the function name, such as `isRightTriangle.m`.

It could be used like so...
```matlab
% check a triangle
if isRightTriangle(3, 4, 5)
    disp('ja!'); 
else
    disp('nein!');
end
```
or like so...
```matlab
%% find right angle triangles for integer-sided triangles < 25
maxLength = 25;

% assume A <= B < C since:
%    hypoteneuse (C) is longest side
%    triangle A-B-C is the same as triangle B-A-C.

for A = 1 : maxLength
    for B = A : maxLength
        % simplified
        for C = B+1:maxLength

            if isRightTriangle(A, B, C)
                disp([A B C]); 
            end
        end
    end
end
```
