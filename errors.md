# Common Error Messages

## Error terminology

Words that you see commonly in error messages:
* [input_argument](glossary.md#input_argument)
* [output_argument](glossary.md#output_argument)
* [array](golossary.md#array)
* [index, indices, indexing](glossary.md#index)
* [error](#error_definition)
* [warning](#warning_definition)

## Common Error examples

### `Array indices must be positive integers or logical values.`

d.H. **Array-Indizes muss Ordinalzahl (erste `1.` , zweite `2.` , usw) sein. Sie kann auch ein Logical Array _von gleichen Grosse_ sein**

```matlab
% Mit dieser Getraenke Array
Getraenke = ["Kaffee" "Tee" "Wasser"]; 

% Diese sind gültig
Getraenke(2);        % "Tee", das heisst, das zweite Getränk von der Liste
Getranenke([1,3])    %  "Kaffee" und "Tee"
Getraenke([false false true]) % nur "Wasser"

% Diese erzeugen Fehler
Getraenk(1.5); % error, weil 1.5 is keineDiese braucht ein Ordinalzahl 
Getraenk(0); % error, weil es gibt keine nullter Ort
```

### `Dot indexing is not supported for variables of this type.`

```matlab
a = 5;
a.name
%error! a is a number, but we are asking for a field (valid for struct and classes)
```

### `Error using [...] Too many input arguments.`

```matlab
disp(1,3)
% error! disp takes only one argument
```

### `Error using [...] Too many output arguments.`

```matlab
a = disp(3)
% error! disp does not return a value
```

### `Error using [...] Vectors must be the same length.`

```matlab
a = 1:3;
b = [6 7];
plot(a,b)
% error! plotting a against b, they need to be the same length
```

### `Index exceeds array bounds.`

```matlab
a = 1:3;
a(5)
% error! asking for 5th element of a 3-element array
```

### `Invalid expression. When calling a function or indexing a variable, use parentheses. Otherwise, check for mismatched delimiters.`

```
 (3 + 5))
        ↑
Error: Invalid expression. When calling a
function or indexing a variable, use parentheses.
Otherwise, check for mismatched delimiters.
```

### `Invalid expression. Check for missing multiplication operator, missing or unbalanced delimiters, or other syntax error. To construct matrices, use brackets instead of parentheses.`

```
a2a3b
 ↑
Error: Invalid expression. Check for missing
multiplication operator, missing or unbalanced
delimiters, or other syntax error. To construct
matrices, use brackets instead of parentheses.
```

### `Matrix dimensions must agree.`

```matlab
a = [1 2 3]
b = [4 5]
a + b
% error! vector addition requires both variables to be the same length.
```

### `'NAME' appears to be both a function and a variable. If this is unintentional, use 'clear NAME' to remove the variable 'NAME' from the workspace.`

```matlab
sin = 6;
sin(pi)
% error! the function sin() was overwritten when we created the variable sin
```

### `Undefined function or variable` [...]

```matlab
clear a
a
%error! a does not exist
```
