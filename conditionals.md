# Bedingungen

### 
### Konditionen
Vergleichen Sie zwei Werte mit Vergleichsoperatoren. Diese sind:
* `==` 
* `>`
* `<`
* `>=` 
* `<=`
* `~=` 


#### Beispeil

```matlab
a == b % wahr wenn a hat die gleiche Wert wie b
```

### `if` - `end`

```matlab
if (Kondition)
    % Diese werden nur ausgewertet, wenn die Bedingung wahr ist
end
```
### `if` - `else` - `end`
```matlab
if (Kondition)
    % Diese werden nur ausgewertet, wenn die Bedingung wahr ist
    % wenn fertig, gehe weiter vom END
else
    % iregenetwas zu tun, nur wenn Kondition falsch ist
    % wenn fertig, gehe weiter vom END
end
```

#### Beispiel
```matlab
esRegnet = true
if (esRegnet)
    disp('Es regnet. Ich bringe ich ein Regenschirm mit');
else
    disp('Es regnet nicht. Ich gehe ohne Regenschutz.');
end
```
### `if` - `elseif` - `else` - `end`
```matlab
if (KonditionA)
    % Diese werden nur ausgewertet, wenn die Bedingung wahr ist
    % wenn fertig, gehe weiter vom END

elseif (KonditionB)
    % in hier KonditionA ist falsch, aber KonditionB ist wahr.
    % wenn fertig, gehe weiter vom END

else
    % in hier beide Konditionen sind falsch.
end

```

#### Beispeil
```matlab
% Virwirt beispiel

A=inf;
% Frage für ein Zahl der muss zwichen -3 und 9 sein (inklusive)
while (A < -3 || A > 9)
   A=input('Bitte, Schreibe ein Zahl (-3 bis 9) :')
   A=round(A) % ich will kein Decimal zahl.
end

if (A==0)
    disp('A ist Nul')
elseif (A > 3 && A < 8)
    disp('A ist ein von 4, 5, 6, oder 7')
elseif (A <= 1 || A >= 9)
    disp('A ist ein von -3 -2 -1 1 oder 9')
elseif ( A ~= 8)
    disp('A ist entwieder 2 oder 3')
else
    disp('A ist 8')
end
``` 
> warum beggint dieser Beispiel mit `A=inf` ?  Wass würde passieren, wenn statt `A=inf`, wir nutzen `A=0`? oder kein Zuordnung zu A ?

## `switch` statements (`switch` - `case` - `otherwise` - `end`)

Wenn es gibt viele möglichkeiten, es ist oft klarer zum schreiben mit
`switch-case-otherwise`
Aber, es gibt keine mehr-als, weniger-als Konditionen. es ist nur vergleich.
```matlab
% Virwirt (mit switch/case statt if/elseif/else)

A=inf;
% Frage für ein Zahl der muss zwichen -3 und 9 sein (inklusive)
while (A < -3 || A > 9)
   A=input('Bitte, Schreibe ein Zahl (-3 bis 9) :')
   A=round(A) % ich will kein Decimal zahl.
end

switch A
    case 0
        disp('A ist Nul')
    case {3, 4, 5, 7}
        disp('A ist ein von 4, 5, 6, oder 7')
    case {-3, -2, -1, 1, 9}
        disp('A ist ein von -3 -2 -1 1 oder 9')
    case {2, 3}
        disp('A ist entwieder 2 oder 3')
    otherwise
        disp('A ist 8')
end
```
aber `switch` funktioniert super für strings
```matlab
WetterMoglichkeiten = {'Sonnig und Kalt',...
                        'Sonnig und warm',...
                        'Regen',...
                        'Sturm',...
                        'Gewitter',...
                        'Schnee'};

% Random wahl von Wetter.
heuteWetter = WetterMoglichkeiten{randi(numel(WetterMoglichkeiten)}

switch (lower(heuteWetter))
    case 'sonnig und warm'
        kleider = 'Badanzug';
    case 'sonnig und kalt'
        kleider = 'Pullover';
    case 'schnee'
        kleider = 'Handschue, Schal, und Mütze'
    case {'regen,'getwitter'}
        kleider = 'Gummistiefel
    otherwise
        kleider = '(?)'
end
disp([' Ich trage ', kleider, ' mit.']);

```
> warum `numel(WetterMoglichkeiten)` stat einfach ein Zahl?

> warum `switch (lower(heuteWetter))` statt `switch (heuteWetter`)