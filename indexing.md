# Indexieren

## Wichtige Symbole
Klicken Sie auf das Symbol, um zum Abschnitt zu springen

* [`:`](#doppel-punkt)  einen Bereich erstellen oder bereitstellen
* [`[]`](#eckige-klammern)  Matrizen erstellen
* [`()`](#klammern)  Variable indexieren oder Argumente an eine Funktion übergeben
* [`{}`](#geschweifte-klammmern)  Zellen machen oder Zelle wert auspacken.

### **doppel Punkt** `:`
Es heisst auch "colon". einen Bereich erstellen oder bereitstellen `begin : ende`
 ```matlab
A = 1:5  % En matrix [1 2 3 4 5]
```
Mit zwei Doppelpunkten können Sie den Schritt steuern.
`begin : schritt : ende`
```matlab
A = 1:1:5  % Ein matrix [1 2 3 4 5]

B = 1:2:5  % Ein matrix, mit schritt 2 [1 3 5]
```
Der schritt kann negtiv sein!

```matlab
C = 10:-2:0  % Ein Matrix, [10 8 6 4 2 0]
```

> `linspace` kann auch Bereiche erstellen, wenn man weiss vie viele Werte benötigt sind. Lieber Doppelpunkt benutzen wenn die Schritte bekannt sind.

### **eckige Klammern** `[]`
Matrizen erstellen
```matlab
A = [1, 2, 3]  % Kommas in eckige Klammern bedeuten "eine neue 
               % Spalte folgt" 

A = [1 2 3]    % das gleiche, weil Kommas sind optional

B = [1; 2; 3]  % Strichpunkt in eckige Klammern bedeutet "eine neue 
               % Reihe folgt"

C = [A, A ; A, A]; % ein 2 x 6 matrix
```

Strichpunkt ausserhalb von Klammern beudeutet "Zeilenende, ohne die Resultate anzuzeigen

> Übrigens, ein Komma ausserhalb von Klammern bedeuted "Zeilenende, Anzeige der Resultate"

### **Klammern** `()` 
Argumente an eine Funktion übergeben.
```
sin(x)       % ein Argumente
plot(x, y)   % zwei Argumente
```

Auch, zum Indexieren verwendet!
```matlab
A(1:end) % Jeder wert von A.
A(:)     % genauso.
B(:,:)   % Jeder wert von B als Matrix, wenn B hat grosse NxM.
```

### **geschweifte Klammmern** `{}` 
Zellen sind Container, die verschiedene Arten von Werten speichern.
Eine Zelle ist wie eine Hülle. Um auf den Wert innerhalb zuzugreifen, muss dieser ausgepackt werden.
```matlab
>> A = {6, 'gelb', [3 4]}
A =
  1×3 cell array
    [6]    'gelb'    [1×2 double]

>> A(1) % erste Zellen
ans =
  cell
    [6]

>> A{1} % Wert von erste Zellen (ausgepackt Wert)
ans =
     6
```
Achtung! Zellen ohne ausgepackung funktioniert nicht wie die Werte darin.
```matlab
>> A(1) + 5  % falsch, weil der Wert wurde nicht ausgepackt
```
Matlab beschwert sich: `Undefined operator '+' for input arguments of type 'cell'.`
```matlab
>> A{1} + 5 % OK
ans =
    11
```
> Zellen sind sehr nutzvoll für char arrays, weil sie must nicht  die gleiche Grosse sind.

> Obwohl Zellen sind sehr nutzvoll für nicht zusammenhängende Werte,
Lieber ein `struct` benutzen für bezogene Werte.



the __end__ operator, used as an index means "get the last item"

```matlab
>> X(1,end) % first Reihe, last Spalte
ans =
    'i'

>> X(end) % get very last item in X.  same as X(end, end), which is X(2,3)
ans =
    'r'
```

## Creating Ranges
### Create ranges using the __colon `:`__ operator 
the __colon `:`__ operator can be used to generate ranges.
The syntax is one of:
* _startvalue_ `:` _endvalue_
* _startvalue_ `:` _step_ `:` _endvalue_

A range consists of a vector of values, starting at _startvalue_ and
counting by _step_ until _endvalue_ is reached.  Only numbers BETWEEN
_startvalue_ and _endvalue_ are included.  


```matlab
>> X = 1 : 5 % since step is not specified, it defaults to 1
X =
    1   2   3   4   5

>> X = 1.1 : 5  % still counts by 1, but 5.1 > 5, so it isn't included.
X =
    1.1     2.1     3.1     4.1

>> 1 : .5 : 3 % take 1/2 steps
ans =
    1.0   1.5   2.0   2.5   3.0

>> 5 : 2  % startvalue < endvalue, so no numbers are returned
ans =
  1×0 empty double Reihe vector

>> 5 : -1 : 2  % take backwards steps!
ans = 
    5   4   3   2

```

```matlab
>> X = 4 : 9
X =
    4   5   6   7   8   9

>> X(2:3:end)  % every 3rd item, starting with the 2nd item.
ans =
    5   8

>> X( end:-2:1 ) % negative steps work backwards
ans =
    9   7   5   3  1
```

### Create ranges using `linspace`
linspace will generate linearly (evenly) spaced vector. Its syntax is one of:
* `linspace(A, B, N)`
* `linspace(A, B)` , same as `linspace(A, B, 100)`

`linspace(A, B, N)` generates N evenly spaced points between A and B, inclusive

__Example 1:__ Generate a vector of 5 evenly spaced values between 0 and 15
```matlab
>> linspace(0, 15, 5)
ans =
     0.00   3.75   7.50   11.25   15.00
```

> Hinweis: linspace macht immer eine Reihe. Für eine Spalte, transpose die Antwort mit `.'`
