# MATLAB Datentypen

## Allgegenwärtig Datentypen (Sie werden diese immer benutzen)

__Klicken Sie__ auf einen beliebigen Datatypnamen für Details

* [__double__ (Gleitkommawert mit doppelter Genauigkeit)](datatypes/double.md)

    Ein typische Zahl,  z.B. `0`, `23` , `1.5E-4` , `-23.5`.

  ```matlab
  >> num = 1.5             
  >> 3 + 5.0 * 12.3 - 14 ^ num 
  ```

* [__char__ (Zeichen)](datatypes/char.md)

    Dies stellt Zeichen und Wörter wie `'?'`, `'Hund'`, and `'Das ist ein Satz'`.  Mehr als ein Zeichen wird als "character array". bezeichnet. Sie machen Text zu einem Zeichen, indem Sie ihn in einfache Anführungszeichen [`'`] setzen.

  ```matlab
  >> c = 'Buchstaben'
  >> length(c)          % 10
  >> 'AB'+'CD'          % [131 133] (huh? velleicht nicht wie erwartet!) 
  ```

* [__string__ (Zeichenfolge)](datatypes/string.md) 

    Spezialisierte Zeichen-Arrays, die char für viele Zwecke langsam ersetzen. Verwenden Sie doppelte Anführungszeichen [`"`], um eine Zeichenfolge zu erstellen.

    ```matlab
    >> s = "die Zeichenkette"
    >> length(s)       % 1
    >> "AB" + "CD"     % "ABCD"
    ```

* [__logical__ (logisch)](datatypes/logical.md)

    Dies stellt entweder 'wahr' (`true`) oder 'falsch' (`false`) dar und wird für Vergleiche verwendet.

    ```matlab
    >> wahr = true
    >> falsch = false
    >> falsch || ((wahr ~= falsch) && falsch == falsch) % true!  || heisst ODER , && heisst UND
    ```


## Andere häufige Datentypen
* [__cell__ (Zellen)](datatypes/cell)

    a _cell_ is a container that can hold a variety of unrelated types.

* [__struct__ (Struktur)](datatypes/struct)

    a _struct_ is a way to gather related quantities together in named fields. 
    
* [__datetime__ (Datum und Uhrzeit)](datatypes/datetime.md)

    _datetime_ represents a moment in time, such as right now, a year ago, or the moment you were born.

* [__duration__ (Dauer)](datatypes/duration.md)

    _duration_ represents a length of time, such as 5 days, 20 seconds, or 300 years.

### Beispiele
```matlab
>> sachen = {1, 'Ein'}; % 1x2 cell, mit verschidene Datatype
>> jetzt = datetime;  % datetime: dieser Moment
>> kurtze_zeit = minutes(5); % duration: funf Minuten
>> etw_frueher = jetzt - kurtze_zeit; % datetime: vor funf Minuten
>> zwischenzeit = datetime - jetzt; % duration: Zeit, der vorbei ist.
```

## Other Specialized Types

* [__categorical__ (kategorisch)](datatypes/categorical.md)

Kategorien repräsentieren eine begrenzte Anzahl von Werten und ermöglichen eine effiziente Verarbeitung und statistische Abfragen.

* [__table__ (Tabelle)](datatypes/table.md)


* [__containers.Map__ (Datenwörterbuch)](datatypes/mapcontainer.md)

Enthält Werte, die mit einem "Schlüssel" abgerufen werden können

