# Figuren
So funktioniert Matlab Figuren:

eine [Figure](#figure) can ein oder mehr [axes](#axes) haben.
Man kann einer axes definieren mit [`axes`](#axes) oder mehrmal
axes machen mit [subplot](#subplots).

To plot data in a series, use `plot` or `plot3`.  For data that is descrete, use `scatter` or `scatter3`.

## Wichtige funktionen:
#### figure
`figure()` neue Figur erstellen

`f = figure()` Erstelle eine Figur und behalte sie einen Griff.

`figure(property, value)`  Erstellen Sie eine Figur und geben Sie ein Eigenschaftswertepaar an.   Zum Beispiel `figure('name','myFigure')` macht  eine Figur und ändert seinen Namen in 'myFigure'.

#### gcf
`f = gcf` Holen Sie sich einen Griff zur aktuellen Figur. Mit diesem Handle kann die Figur später geändert werden

#### axes
`axes()` neue Achsen in der aktuellen Figur erstellen

`axes(f)` neue Achsen in der angegebenen Figur *f* erstellen

`ax = axes()` neue Achsen in der Figur erstellen, und Holen Sie sich einen Griff zur diesen neuen Achsen.


#### gca
`ax = gca` Holen Sie sich einen Griff zur aktuellen Achsen. Mit diesem Handle kann die Achsen später geändert werden

#### subplot
`subplot(Reihe,s,n)` mehrere Achsen in einer einzigen Figur zu erstellen.
see [subplots](#subplots)
#### plot
`plot(x,y)`

`plot(x,y,linespec)`

#### plot3
`plot3(x,y,z)`

#### scatter
`scatter(x,y)`

`scatter(x,y, grosse, farbe)`

#### scatter3
`scatter(x,y,z)`

`scatter(x, y, z, grosse, farbe)`

#### bar

#### histogram

## subplots
`subplot( Reihe , Spalte, welchePosition)`
![subplots](resources/fig_series/subplots.png)


# plot types
![plot typen](resources/fig_series/plottypes.png)
# labeling axes
![labeling](resources/fig_series/labeling.png)


## modifying axes
![example of axes changes](resources/fig_series/axes_changes.png)
