# Basic MATLAB syntax

## The Matlab Command line
Commands can be typed directly into matlab. 

#### Prompts
The prompt is where you typically will run commands. It looks like a pair of "greater-than" symbols.
```matlab
>>
```

Sometimes, the prompt will be preceeded by a "K".  This is the "debug" prompt, useful for stepping through a program, looking at values along the way.
```matlab
K>>
```
To exit this mode, type `dbquit` and then press enter `↵`
```matlab
K>>dbquit ↵
```

### Math at the command line
Matlab can be used as a fancy calculator
```matlab
>> 5 + 2 ↵
ans =
     7
>> cos(pi) ↵
ans =
    -1
```
the result `ans` can be reused in the next calculation. (ans is a variable)
```matlab
>> ans / 4 ↵
ans =
   -0.2500
```
## More than one item : Vectors and Matrices
```matlab
>> [3 5] ↵
ans =
     3     5

>> [3, 5] ↵
ans =
     3     5

>> [3; 5] ↵
ans =
     3
     5
```
### ranges
```matlab
>>
```
### subject
```matlab
>>
```