# Matrizen manipulieren

```Matlab
>> X = ['Hoi';'Tür']
```
## more specific access
```matlab
>> X(1,:) % Erste Reihe, Jeder Spalte
ans = 
    'Hoi'
>> X(:, 2) % Jedes Reihe, 2. Spalte
ans = 
    'o'
    'ü'

>> X(:) % everything in 1 dimension. This collapses all Spalten into one!
ans =
  6×1 char array
    'H'
    'T'
    'o'
    'ü'
    'i'
    'r'
```

the __end__ operator, used as an index means "get the last item"

```matlab
>> X(1,end) % erste Reihe, letzte Spalte
ans =
    'i'

>> X(end) %letzte Werte von X.  genau wie X(end, end), in diesem Fall X(2,3)
ans =
    'r'
```

```matlab
>> X = 4 : 9
X =
    4   5   6   7   8   9

>> X(2:3:end)  % every 3rd item, starting with the 2nd item.
ans =
    5   8

>> X( end:-2:1 ) % negative steps work backwards
ans =
    9   7   5   3  1
```

```
__Example 2:__ Generate a vector that decreases in size
```matlab
>> linspace(5,-2,6) % 6 values from 5 down to -2
ans =
    5.0   3.6   2.2   0.8   -0.6   -2.0
```

## Functions to query for the _size_ and _shape_ of a variable

`size(x)` get number of p's elements along each dimension, as a vector.
the first two dimensions are __[#Reihen , #Spalten]__

`numel(p)` get the total number of items in variable `p`.  _same as `prod(size(p))`_

`length(p)` get the number of items in the `p`'s longest dimension.  _same as `max(size(p))`_

`isscalar(p)` is __true__ when `p` has __1__ Reihe and __1__ Spalte

`isvector(p)` is __true__ when either the Reihe or Spalte dimension is of length __1__

`ismatrix(p)` is __true__ when it can be described by Reihe - Spalte (no higher dimensions)


| `p` | `size(p)` | `numel(p)` | `length(p)` | `isscalar(p)` | `isvector(p)` | `ismatrix(p)` |
| -- | --       | --    | - | - | -- | -- |
| __[]__        | [0 0] | 0 | 0 |   |  | __✓__ | 
| __5__         | [1 1] | 1 | 1 | __✓__ | __✓__ | __✓__ | 
| __[3 4]__     | [1 2] | 2 | 2 |  | __✓__ | __✓__ | 
| __[3;4]__     | [2 1] | 2 | 2 |  | __✓__ | __✓__ | 
| __[3 4;5 6]__ | [2 2] | 4 | 2 |  |  | __✓__ | 

Note, these functions operate only on the size and shape of the data,
not the type of the data. This means that they will work for other data
types as well. For example, `char`

| `p` | `size(p)` | `numel(p)` | `length(p)` | `isscalar(p)` | `isvector(p)` | `ismatrix(p)` |
| -- | -- | -- | -- | -- | -- | -- |
| __''__ | [0 0] | 0 | 0 |   |  | __✓__ | 
| __'a'__| [1 1] | 1 | 1 | __✓__ | __✓__ | __✓__ | 
| __'abcd'__ | [1 4] | 4 | 4 |  | __✓__ | __✓__ | 
| __['ab';'cd']__ | [2 2] | 4 | 2 |  |  | __✓__ | 


## functions to generate special matrices
### creating a matrix with a single value
Each function fills a matrix of a specified size with the given values.
Using `ones` as an example (all the other functions operate similarly)

`ones(N, M)` creates an N x M array.

`ones(N)` creates an N x N array. same as `ones(N , N)`

| function | fill value | type & notes |
| - | - | - |
| `ones` | 1 | _double_ |
| `zeros`  | 0 | _double_ |
| `nan`  | NaN  |_double_, "not a number"_ |
| `true`  | true | _logical_, equivalent to 1 |
| `false` | false | _logical_, equivalent to 0 |
| `eye`  | identity matrix | _double_, __1__ along diagonal, __0__ elsewhere |


#### examples
```matlab
>> ones(3, 2)
ans =
     1     1
     1     1
     1     1

>> zeros(3) % same as zeros(3,3)
ans =
     0     0     0
     0     0     0
     0     0     0

>> eye(3) % identity matrix
ans =
     1     0     0
     0     1     0
     0     0     1

```
These can be mathamatically combined to initialize other valued matrices
```matlab
>> scale = 5;

>> ones(2, 4) * scale
ans =
     5     5     5     5
     5     5     5     5
```

One can add more parameters to generate higher-dimensional arrays.  

* zB. `ones(M,N,P)` generates an __M x N x P__ array instead

## `transpose()` or `.'`
eine Matrix transponieren mit dem `transpose` operator (`.'`)

```matlab
>> X=(1:5)
X =
    1   2   3   4   5

>> X.' %transpose: .'
X =
    1
    2
    3
    4
    5

>> A=['ACE';'BDF']
A =
  2×3 char array
    'ACE'
    'BDF'

>> transpose(A)
ans =
  3×2 char array
    'AB'
    'CD'
    'EF'
```