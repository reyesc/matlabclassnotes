# LOOPS
* [for](#for-loop) : Wenn man etwas ein bestimmt Anzahl wiederholen.
* [while](#while-loop) : Wenn man möchte etwas wiederholen, bis eine Bedingung erfüllt ist. "While" ist selten gebrauched, lieber "for" nutzen.

## `for` loop
Die **`for`-Schleife** kann ein prozess wiederholen, wenn ein Anzahl zum wiederholen ist vorgegeben.
```matlab
for var = RANGE
    % Hier geht was ich mochte mehrmahl evaluieren
end
```
![For Loop Diagram](resources/forloop.png)

Hier, `Range` ist einfach ein Vektor. [z.B. Bereich definieren](indexing.md#doppel-punkt)
```matlab
% print values 1 : 5
for value = 1:5
    disp(value)
end
```

### `For` **gemeinsame Nutzung**: verarbeiten jedes Element eines Vektors
```matlab
values =[1 2 3 5 7 11];
for n = 1 : numel(values)  % ein mal fur jeden zahl in "values"
    dieserValue = values(n);
    dieserAntwort = irgenFunktion(dieserValue);
    alleAntworten(n) = dieserAntwort;
end

% am ende habe ich:
% n : 6 (der letzte Position in "values")
% dieserValue : 11 (der letzte zahl in "values")
% dieserAntwort : der wert von irgenFunktion(11)
% alleAntworten : ein Vektor, der alle Antworten enthält -> super für Plots.
```

## `while` loop

Verwenden Sie eine **`while`-Schleife**, um einen Prozess eine unbekannte Anzahl von Malen zu wiederholen, während eine Bedingung wahr ist.

```matlab
while (Kondition)
    % was ich mochte mehrmal machen
end
```
Die Bedingung ist etwas die als entwieder falsch oder eckt evaluiert kann.  Hier gibt Bedingung (Kondition) beispiele.
```
x == 0     % wahr, wenn x ist geneau 0
y > 3      % wahr wenn y ist grosser als 3
n <= 45    % wahr, wenn n ist 45 oder weniger
isempty(q) % wahr, wenn size(q) in eine richtung ist 0. zB. '' oder [] 
length(G) < 23 % wahr, wenn der grosse von G (vie viele platze) ist weniger als 23
```

### `while` Beispiel:
```matlab
% Wie oft muss ich eine Münze umdrehen, um 5 Köpfe zu bekommen?

kopf = 0; % bis jetzt, wir haben keine Köpfe zu bekommen
flips = 0; % wir haben noch nicht es umdreht
Ziel = 5; % Köpfe

while (kopf < Ziel)

    % Drehen Münze um.
    flips = flips + 1; 
    istKopf = randi([0 1])==1; %  ( 0 = Schwanz, 1=Kopf )

    % istKopf ist eintwieder 1 (wahr) oder 0 (falsch).
    % deshalb, ich kan einfach das Ergebnis hinzufügen to kopf
    kopf = kopf + istKopf; 
end

disp(['Es braucht ' num2str(flips) ' umdrehungen um ' num2str(Ziel) ' Köpfe zu bekommen'])
```
> wie würde ich es ändern, wenn ich Schwanz statt Kopf finden?