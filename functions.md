# Common Functions

## MOST COMMON

* __disp__ display something.. text, contents of a variable, etc.
```matlab
>> x=5;
>> disp(x)
5
>> disp('Millenium Falcon')
Milenium Falcon
```

* __linspace__
* __figure__
* __hold__
* __xlabel__, __ylabel__
* __title__
* __grid__
* __plot__ plot something



## WORTH LEARNING EVENTUALLY
* __fprintf__ formatted printing either to a file or to the screen
* __sprintf__ formatted printing that writes to a variable

# Plot-Related Functions
## How does MATLAB plotting work?

### the following examples will use this data
```matlab

% set up some data to plot
x = [0 : 0.1 : 30];
y1 = sin(x);
y2 = sin(x + 0.5)
```

### simplest plot example
Plotting is a simple process in matlab
```matlab

% create an empty figure.
figure

% plot something.  This will automatically create a set of axes in the current figure
plot(x, y1)
```
###

```matlab

% This may contain one or more axes.
% it also can have its own menubar and toolbar
fig = figure('Name','MyFigure')

% this is now the current figure. 

% create an axes within the figure
ax = axes

% plot values, 
pl = plot(ax, x, y, 'o-');

[diagram of a plot]
### Parts
The large frame around