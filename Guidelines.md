# Guidelines

* Numbers without context are meaningless
* Names should make the code readable

## Use descriptive variable names
To make it that your brain has to juggle fewer items, name your variables deliberately.  

### Example: calculating stress
For example, suppose we have the following assignment:

`"compute stress exerted on the floor, from filling a bathtub (1.5m long x 0.8m wide x 0.5m deep) with sand (1500 kg/m^3)."`

One solution:

```matlab
% compute stress for bathtub filled with sand
result = (1.5 * 0.8 * 0.5 * 1500 * 9.81) / (1.5*0.8));
```

This works, but tells you nothing about the logic behind what you're doing. Where did that 9.81 come from, anyway?

A better solution acknowledges the equations behind your math.

```matlab
g = 9.81;            % N/kg
V = 1.5 * 0.8 * 0.5; % m^3
rho = 1500;          % kg/m^3
F = V * rho * g;     % newtons
A = 1.5 * 0.8;       % m
sigma = F / A;       % Pa
```

Now, you can at least check whether your calculations make sense.

However, this is still more abstract than it needs to be.  Without reading the original instructions,we don't get a sense of what this program is actually calculating.  Also, why use `sigma` when we really mean `stress`, and why use `rho` when we actually mean `density`

even better... this code stands completely on its own!

```matlab
% all measurements in SI units: meters, kg, N, Pa
gravity = 9.81 ;        % N/kg

% dimensions of the Tub, in meters
Length = 1.5;
Width  = 0.8;
Depth  = 0.5;

volume    = Width * Length * Depth;
footprint = Width * Length;

materialDensity = 1500;         % value for sand
totalMass = materialDensity * volume;
stress_Pa = (totalMass * gravity) / footprint;
```

Now, suppose one of these programs were provided to you, along with the instructions:

_"Use this code to estimate the stress that a 3 story  building filled with jellybeans exerts on the ground.. on MARS"_

Which version would be easiest to understand and modify?

## Take-home message
You will almost ALWAYS find yourself going back and modifying programs. 

The clearer your logic is expressed, the easier it is for you (or others) to ...
* **detect logic errors**
* **understand** what your code does
* **modify this code** to suit your changing needs.