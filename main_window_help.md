# Quick overview of The MATLAB Environment
Most programming will be done in the MATLAB environment.
![Figure 1-1](resources/basic_window.png) "Figure 1.1"
The main windows

* **(A) Command Window** : Here, you type each command, one after the other. The computer performs each operation as soon as you have entered it.  Results appear in this window, but any variables you create are put in the Workspace window
* **(B) Workspace Window** : here you will find a list of the variables you've been using.  You can also click on them for more details
* **(C) Editor window** : When you write a script or function, you may want to use the editor window. Although you could use any text editor, creating files in this window allows MATLAB to parse your files, offer suggestions, and do automatic formatting.  
When MATLAB thinks you might be doing something inefficiently or incorrectly, it will underline things with an orange squiggly line. 
When MATLAB knows it will not be able to understand your program, it will use a red squiggly line. Either Hovering over the item with your mouse, or right-clicking on the underlined item will provide more details as to what might be the error, and possible ways to fix it.
* **(D) Current Folder window** : This will show the current folder and directory. you can navigate your file system from here.  Folders and files in gray are unknown to matlab, since they are not on its "search path".  Whenever you try to run a script or function, matlab looks for matches in a list of places known as a "search path".  You can add a folder to the search path by right-clicking and choosing "Add to path"

# Working in MATLAB
![Figure 1-2](resources/matlab_function.png) "Figure 1.2" Working with a function

## `(A)` Customize your Layout
You can customize your MATLAB window via the  "Layout" control or by dragging parts around on screen.  This is why this figure will look different than your screen, but the parts are all there, just moved around. 

## `(B)` Editor screen, with a sample function

B is the Editor, where you will do most of you work.

Functions have a few basic parts.  The first line provides the function name, and lists input and output arguments, if there are any. Here is a simple example

```matlab
function sayhello()
  % sayhello schreibt ein Grüsse
  disp('hoi')
end
```

### Function declaration (the first line)
```matlab
function [surfaceArea, volume, surfaceToVolumeRatio] = cubeMessung( x )
```
This function takes a single *argument* (`x`) and returns three values `surfaceArea`, `volume`, and `surfaceToVolumeRatio`

Each **argument** is a value that is provided to the function, or that the function returns.

### Function Help (the first block of comments)


## `(C)` Searching for help
the `lookfor` command will search the first comment line of every documented function for the term you specify. In this example, `lookfor cube` searches for the word "cube" in the first comment of every file.  
```
>> lookfor cube
cubeMessung                    - returns the surface area and volume for a cube
soma                           - display precomputed solutions to Piet Hein's soma cube
somasols                       - Solutions to the SOMA cube.
 ...
```
The results are clickable links that will display additional help for each of the found functions.

## `(D)` Get detailed help
The `help` command will display more help for the function.  As you can see from the _cubeMessung_ example, help returns the first block of comments.
```
>> help cubeMessung

  cubeMessung returns the surface area and volume for a cube
 
  [surfaceArea, volume] = cubeMeasurements(length) will return the 
  surface area and volume for a cube where all edges are of length
  units will be related to the input units.
 
    surfaceArea will be in units^2 
    volume will be units^3
    ratio will be units
```
>what if there were an empty line (not even a comment symbol `%`) somewhere inside this block?

## `(E)` Getting results from a function
Here, the `cubeMessung` function is called, with the results being placed into the variables `area`, `vol`, and `ratio`
```
>> [area, vol, ratio] = cubeMessung(1)

area =
     6

vol =
     1

ratio =
     6
```

Notice that the variable names inside the function do not have to match the variable names outside the function!  The variables inside the function are isolated from the rest of the program.

> what kind of trouble could arise if function variables weren't isolated?

## `(F)` Workspace variables
The answer from the call to cubeMessung is now in the workspace. Notice also, that the variables used inside the function do not show up.

## `(G)` More help
Matlab can provide information on how to use a function. After typing a valid function name, and opening the parenthesis, matlab can show how the function was defined. Typing the name and hitting `F1` would also bring up additional help.

## `(H)` Even more help
Here, again, is the details provided by the function's first comments.  This is what you might see when you type `doc cubeMessung`