%% Introduction to 2-D Plotting


%% create data for these plots
x = 0:30;   
y1 = sin(x / (pi));
y2 = 2 * cos( x / (pi)) - rand(size(x));

%% default behavior (absolute simplest case)
% create a plot in the current axes of the current figure.
% if a figure doesn't exist, then automatically create it

% plot some data
plot(x,y1);

pause(5);  % wait 5 seconds

% plot the other data
plot(x,y2)

% notice the old plot was replaced by the new one
%% manipulating axes