%% Clock sample

duration_1s = seconds(1);
duration_1m = minutes(1);
duration_1h = hours(1);
clear clockhand;

clockhand.name = 'hour';
clockhand.period = hours(12);
clockhand.length_cm = 3;
clockhand.width_px = 2.5;

clockhand(2).name = 'minute';
clockhand(2).period = minutes(60);
clockhand(2).length_cm = 4;
clockhand(2).width_px = 1.5;

clockhand(3).name = 'second';
clockhand(3).period = seconds(60);
clockhand(3).length_cm = 2.5;
clockhand(3).width_px = 1;

handX = @(hand, when) sind( 360 .* when ./ hand.period) .* hand.length_cm;
handY = @(hand, when) cosd(360 .* when ./hand.period) .* hand.length_cm;
% vector representing every second of time from t=0 to t=12 hours
sampleTime = hours(0) : seconds(5) : hours(12);
mynow = datetime;
hms = [mod(hour(mynow),12) minute(mynow) second(mynow)];
for h=1:3
    plot3(handX(clockhand(h),sampleTime),handY(clockhand(h),sampleTime),sampleTime,'Linewidth',clockhand(h).width_px)
    hold on
    
end
grid on
hold off
legend({clockhand.name})