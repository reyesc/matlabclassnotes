clear all
%% Title of code section, lines that begin with two percent signs 
% Regular comments are text
% Each section is accessible from the EDITOR tab, under "Find"
% Each section can be run individually using EDITOR->"Run Section" or EDITOR->"Run and Advance"

%% Basic Types
% A "Type" describes a variable and determines how it can be used.

count = 23; % numeric type. "double"
imagine = 1i; % complex number. "double(complex)"
letter = 'a'; % single character. "char"
isBig =  true; % either true or false  "logical"

goodDay = datetime(2017,09,23) ; % contains a moment in time "datetime"
waitTime = hours(1.5); % contains a length of time "duration"

%% combining data
% Data can be combined into vectors ( |1xN| or |Nx1| ) and matrices ( |NxM| ) 
% using brackets |*[]*| and braces |*{}*|
%
% Items separated with commas |_,_| are put in a new column, while items separated with
% semicolons |_;_| are added to new rows.

%% combining with [ ] for simple arrays
% requires all items be of the same type

[1, 2, 3] % row
['a', 'b', 'c']
%%
[4; 5; 6] % column
%%
[0, 1 ; 2, 3] % a 2x2 matrix.

%%
% sizes are important and must match. The following would be an error
% 
%   ['abc' ; 'de']
%

%%
% *_Dimensions of matrices being concatenated are not consistent_*

%% combining with { }, for cells
% cells can hold a mix of types. That includes matrices of incompatible sizes.
% Cells may be thought of as containers that you put values into,
% and have to take values out of.  To put values INTO a cell, wrap them with *|{ }|*
stuff = { 'hi', 3.14, true}
%%
stuff = [{'gruetzi'}, stuff] % must be wrapped to add a value to the cell array
%%
% when one access a cell using simple |*( )*|, then cell of another size is retrieved.

% when one access a cell using simple |*{}*|, then you get the value contained inside the cell
stillPacked=stuff(1)
unPacked=stuff{1}
whos stillPacked unPacked

%% struct for grouping and naming data
% with a struct, you are grouping variables together.

person.name = 'joebob'; % create a "struct", which is a variable that contains other variables, called "fields"
person.age = 23;      % add another field to the struct. like a cell, this can be a different type.
person.favorite_numbers=7;

% the next person can be added

person(2).name = 'maryjane';
person(2).age = 24;
person(2).favorite_numbers=[4 13];

disp(person)
whos person
%%
% show each person's details
for i=1:numel(person) 
    disp(['person # ' num2str(i), ' details:'])
    disp(person(i)) 
end

%%
% fields can be aggrogated. To get each person'sname:
names= {person.name}  % as cell, because it keeps names separate,and they might be different lengths
ages = {person.age]

%%
% what would happen if we used | *[]* | to combine names?
[person.name]