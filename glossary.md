# MATLAB Glossary

* [argument](#argument) [(input)](#input_arguments) [(output)](#output_arguments) | [array](#array) | [axes](#axes)
* [categorical](datatypes/categorical.md) | [cell](datatypes/cell.md) | [char](datatypes/char.md) | [class](#class) | [colon ":"](#colon) | [column](#column) | [command](#command) | [command line](#command_line)
* [datetime](datatypes/datetime.md) | [double](datatypes/double.md) | [duration](datatypes/duration.md)
* [error](errors.md#error) | [execute](#execute)
* [field](#field) | [figure](#figure) | [function](#function) | [function handle](#function_handle)
* [handle](#handle)
* [indexing](indexing.md) | [integer](#integer)
* [logical datatype](datatypes/logical.md) | [loop](loops.md)
* [method](#method)
* [object](#object)
* [parameter](#argument) | [property](#property)
* [row](#row) | 
* [scope](#scope) | [script](#script) | [semicolon ";"](#semicolon) | [statement](#statement) | [string](datatypes/string.md) | [struct](datatypes/struct.md)
* [table](datatypes/table.md) 
* [undefined](#undefined)
* [variable](#variable)
* [warning](errors.md#warning) | [workspace](#workspace)


### argument

ein Wert, der an eine Funktion übergeben oder zurückgegeben wird. es kann auch als `Parameter` bezeichnet werden

```matlab
[outarg1, outarg2] = myfunction (inarg1, inarg2, inarg3)  % ein Beispiel
```

#### input arguments

Jede Funktion hat eine vordefinierte Anzahl von Eingabeargumenten, die sie akzeptieren kann. Sie kann auch `Parameters`  genannt werden

```
>> help linspace
linspace Linearly spaced vector
    linspace(X1, X2) generates a row vector of 100 linearly
    equally spaced points between X1 and X2

    linspace(X1, X2, N) generates N points between X1 and X2.
    ... usw...
```

Das heisst, `linspace` akzeptiert entweider **zwei** oder **drei** Argumente.

Some functions can accept any number of arguments.

#### output arguments

Jede Funktion hat eine vordefinierte Anzahl von Ausgabeargumenten, die zurückgegeben werden können.

```
>> help max
max   Largest component
   For vectors, max(X) is the largest element in X....
   ...
   [Y,I] = max(X) returns the indices of the maximum values in vector I. ... 
```

Das heisst, `max` can bis zu **zwei** verschiedene Variablen zurückgeben

### array

Dieser Oberbegriff bezieht sich auf N-dimensionale Arrays, die alles von einem leeren Array zu einzelnen Zahlen ("Scalar" genannt), zu Vektoren, zu Matrizen und zu mehrdimensionalen Matrizen bedeuten können.

```matlab
leer     = [];        % Grosse : 0x0
skalaren = 1;         % Grosse : 1x1
vektor   = [1,2,3]    % Grosse 1x3, ubrigens [1;2;3] ist Grosse 3x1
matriz   = [1 2; 3 4] % grosse 2x2
```

### axes

### class

### colon

doppelpunkt `:` Der Doppelpunkt ist ein Bereichsoperator. Wenn es als Index verwendet wird, bedeutet es "alles" (z.B. `A(:)`).  Wenn es mit Zahlen verwendet wird (z.B. `1:3` oder `0:.2:4`), repräsentiert es einen Bereich.

see [Ranges(Bereich)](indexing.md#Creating_Ranges)

### column 

Spalte, die zweite Dimension einer Matrix. 
`size(X,2)` gibt die Spaltezahl von X.

### command

befehl

### command line

Befehlszeile

Hier geben Sie Befehle direkt in MATLAB ein. Jeder Befehl wird ausgeführt, nachdem er eingegeben wurde. Die Eingabeaufforderung command sieht im Allgemeinen aus wie `>>`, aber wenn ein Programm angehalten wird, kann es `k >>` sein

### command prompt

see [command line](#command_line)

### execute

### field

### figure

### function

Funktionen sind kleine Programme, die eine Aufgabe ausführen. Sie können Eingabeargumente verwenden und einen oder mehrere Werte zurückgeben, müssen dies jedoch nicht tun. z.B:

```matlab
% Dieses Programm wird in der Datei imBereich.m gespeichert
function jaNein = imBereich(X, mindestwert, hoechstwert)
    jaNein = X >= mindestwert & X < hoechstwert
end
```
Und wir nutzen es so:
```matlab
% jaNein könnte entweder von der Befehlszeile oder von einer anderen Funktion oder einem anderen Skript aus aufgerufen werden

jaNein(3, 1, 5); % Die Funktion wird wahr zurückgeben
jaNein([2 5], 1, 5) % Die Funktion wird [wahr falsch] zurückgeben
```


### function handle

### handle

### integer

### method

Eine [Funktion](#function), die zu einer Class gehört. z.B, ein `Bankkonto` Klasse könnte die Methoden `zuruecknehmen` oder `uebertragen` haben.

see also [class](#class)

### object

Ein Objekt ist eine [Variable](#variable), die aus einer Klasse erstellt wurde. Statt `double` oder `char`, es hat einer eine benutzerdefinierte Klasse, wie `Bankkonto` oder `Erdbebenkatalog`

see also [class](#class)


### property

Eine [Variable](#variable) , die zu einer Class gehört. z.B, Ein `Erdbebenkatalog` Klasse könnte die property `Groesse` und/oder `Tiefe` haben.

see also [class](#class)

### row
Reihe, die erste Dimension einer Matrix. 
`size(X,1)` gibt diel Reihezahl von X.

### scope
Der scope (Gültigkeitsbereich) ist die Lebensdauer einer Variablen. 

Variablen, die in Funktionen verwendet werden, haben Lebensdauern, die auf die Funktion beschränkt sind. Sobald die Funktion beendet ist, werden die Variablen automatisch gelöscht. Innerhalb einer Funktion können Sie einen eigenen Satz von Variablen erstellen, ohne sich Gedanken darüber machen zu müssen, ob sie mit Variablen außerhalb von Funktionen in Konflikt stehen.

Eine Variable, die in der Befehlszeile oder in einem Skript verwendet wird, hat eine unbegrenzte Lebensdauer.

Andere, nuanciertere Bereiche existieren, werden hier jedoch nicht angesprochen. z.B. [class scope](#classes) , [handles](#handle)


### script

Textdatei mit Matlab-Befehlen, die das MATLAB-Programm der Reihe nach ausführt.  Alle Berechnungen werden im (globalen) Arbeitsbereich ausgeführt. Dass heisst, wenn das Programm beendet wird (oder einen Fehler verursacht), stehen alle Variablen zur Überprüfung zur Verfügung.

Die Nachteile der Verwendung von Skripts sind:
* Sie können nicht mehrere Variablen mit demselben Namen haben, sonst werden sie einfach überschrieben
* Variablen werden nicht automatisch gelöscht, wenn sie ausgeführt werden, was es leicht macht, Werte von einer Programmausführung zur nächsten zu kontaminieren.  (Deshalb verwenden wir oft "clear")


### semicolon



### statement

### undefined

Undefined bedeuted "**undefiniert**". Das heisst, MATLAB kennt dieser Funktion oder Variable nicht. Es war nicht vorher benutzed und hat keine wert nicht einmal leer.

auch: `undefined function` oder `undefined variable`

### variable

Genau wie eine mathematische Variable ist es ein Symbol, das einen Wert enthalten kann.  Statt `x`, `w`, und `∂`, sie kann lengere Namen haben, wie `longitude`, `Breite`, und `unterschied`. 

### workspace
Arbeitsbereich