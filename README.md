## Index of help topics:
* [Using the MATLAB workspace](main_window_help.md) (EN)
* [Data Types](datatypes.md) (DE/EN mischung)
* [loops](loops.md) (DE): `for` und `while`
* [conditionals](conditionals.md) (DE): `if`-`then`-`else` und `switch`-`case`

* [graphics](graphics.md) (DE/EN mischung): `figure`,`axes`,`subplot`,`plot`,`scatter`, usw.
* [Functions](functions.md)
* [Indexing (:)](indexing.md) (DE/EN mischung)

## some additional topics

* [Style](style.md) (EN)
* [Syntax](syntax.md) (EN)
* [Common Errors](errors.md)

* [Glossary of terms](glossary.md)

## Where am I?
This is GitLab. It is a place where one can keep a record of their programs, and also share programs.  It keeps track of every change made to any file here.

## wass Heist DE? EN?
* **DE** seite auf Deutsch
* **EN** seite bis jetzt auf English