%% Schnell-Schnell Referenz für MATLAB Datatypen

%% Overview : grundlegende Datentypen

% Variable   = Wert     % DataTypen : klein Erklärung
pi           = 3.1415;  % double    : typisch Nummer
derBuchstabe = 'a';     % char      : ein buchstabe. 
wahr         = false;   % logical   : ist nur wahr (true) oder falsch (false)
Gruess = "Hoi zamme!";  % string    : Zeichenfolge
  

jetzt  = datetime();    % datetime  : ein zeitpunkt - ohne argumenten, die resultat is JETZT

dauer  = days(2);        % duration : die Daur. auch bei years, hours, minutes, seconds definierbar

whos

%% Zuzammenspiel zwichen DURATION und DATETIME
fruher = datetime(2018, 7, 20, 14, 30, 0) % definiert mit (Jahre,Monate,Tage,Stunden,Minuten,Sekunden)
%%
stdInTag = days(1) / hours(1)   % DURATION / DURATION is ein DOUBLE (weil es ein Verhältnis ist)

%%
daurSeitFruher = jetzt - fruher % DATETIME - DATETIME ist ein DURATION

%%
morgen = jetzt + days(1) % DATETIME + DURATION ist ein DATETIME

%% STRING und CHAR sind nicht ganz ändliche

%  Im Hintergrund ist jedes CHAR eine Zahl.  z.B. 'A' ist 65, 'B' ist 66, usw.
'abc' + 'def'
%%
'a' == 'abc' % gibt 3 logicsche Werte, weil es vergleiched jeder Buchstaben
%    es wird ein ERROR sein wen die grosse sind nicht kompatibel
%%
% Im Hintergrund ist jede Zeichenfolge eine Gruppe von Zeichen

"Hoi zamme" + ", wie gehts?" % OK. Strings sind nie interpretiert als Nummern.
%%
"a" == "abc" % gibt nur 1 logiche Werte

%% Warum gibt es nicht nur STRING?
% STRING ist ganz neu in MATLAB. Bis vorkurtzen funktioniert alle nur mit CHAR.

