# datatype `double`

Ein `double` ("doppel") representiert Zahlen.


## Beispeile

Any number you type is automatically assumed to be a double.

```matlab
3         % ganze Zahl
3.1415    % Pi
3+4i      % complexe Zahl
6.02e23   % wissenschaftliche Schreibweise 6.02 x 10^23
```

A double can also contain one of several special values:

__NaN__ : "Not a Number", example would be the value of `0/0` where math breaks down.
Look for `nan` values within a dataset using the function `isnan()`

```matlab
>> nichtnummer = 0 / 0
nichtnummer = 
  NaN
```

__Inf__ : positive infinity, example would be `1/0`.  Negative infinity exists, too.
Look for `inf` values within a dataset using the function `isinf()`

  ```matlab
  >> undendlichkeit = 1 / 0
  undendlichkeit = 
      inf
  ```

## Extra infos

### Was meint mann, `double` (`doppel`)?

`double` refers to a `double-precision floating point` number, a description of how that number is stored in the computer.  Depending upon the need, there are a variety ways to store numeric values, and `double` is the default way that MATLAB stores its numeric values.

__in MATLAB__, see the documentation for:

*  `Floating-Point Numbers`
* `Numeric Types`


