# datatype `containers.Map`

Allows you to store values that can be retrieved with a "key"

# Beispiel

```matlab
>>cm = containters.Map()
>>cm('alpha') = 25;
>>cm('beta') = 42;

>>cm('alpha')
ans =
   25
   
>>cm.keys
ans =
    1x2 cell array
   'alpha' 'beta'
```
