# datatype `logical`

Enhalt `wahr` (`true`) oder `falsch` (`false`)

    This represents either true or false, and is used for comparisons.  `false` is equivelent to `0` and `true` is `1`.  
    
    When converting a number to a logical, Matlab treats `0` as `false`, and most any other value as `true`.