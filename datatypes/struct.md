# datatype `struct` (Struktur)
_struct_ is a way to gather related quantities together. Each quantity goes into a named field.

 For example:
    ```matlab
    % declare an earthquake record. Each event has a lat, lon, and depth.
    >> quake.lat = 47.376389; % degrees north
    >> quake.lon = 8.548056; % degrees east
    >> quake.depth = 1; % km

    >> disp(quake)
      lat: 47.3764
      lon: 8.5481
    depth: 1

    >> disp(quake.lat)
    47.3764
    ```
    
### **array of struct** vs **struct of arrays**
#### **array of struct**
```matlab
% script to tie event data together into structs
% raw data for 3 events.
lats = [6.2 8.6 9.3];
lons = [45.2 44.1 48.6];
depths = [15 12 9];

% put them into a struct, so the relationship is clear
for n=1:numel(lats)
    quake(n).lat = lats(n);
    quake(n).lon = lons(n);
    quake(n).depth = depths(n);
end
```
now look at the results
```
>> quake
quake = 
  1×3 struct array with fields:
    lat
    lon
    depth

>> quake(1)
ans = 
  struct with fields:
      lat: 6.2000
      lon: 45.2000
    depth: 15

>> disp(quake(2))
      lat: 8.6000
      lon: 44.1000
    depth: 12
```

#### **struct of arrays**
Useful for clumping data together. In my workspace, I would only have one
variable, with appropriate values.

```matlab
% assign values to a structure named myconsts
myconsts.pi = pi;
myconsts.avogadro = 6.0221409e+23;
myconsts.earthrad_km = 6,371;
myconst.scores = [100 100 100 100 99 100];
```
now look at the results
```
>> disp(myconsts)
             pi: 3.1416
       avogadro: 6.0221e+23
    earthrad_km: 6
         scores: [100 100 100 100 99 100]

>> disp(myconsts.scores(1))
   100
```