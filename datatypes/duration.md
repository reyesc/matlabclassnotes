# datatype `duration`

_duration_ represents a length of time, such as 5 days, 20 seconds, or 300 years.
    durations can be created either by subtracting two datetime values, or by specifying an elapsed amount of time, such as `days(5)`, `seconds(20)`, and `years(300)`
