%% datetime usage

%{
Here's a quick, but hopefully useful overview of the kinds of things that datetime can do.
%}

% old representations

n=now           % n is now a datenum with hours,minutes, seconds
t=today         % t is a datenum representation of day only
dy = 2017.25            % decyear
dt = datetime    % gives the current datetime
daynum = now            % matlab datenum

% datetime(daynum)   % error, requires vector input
curr_time = datetime(datevec(daynum));
disp('today:'); 
curr_day = dateshift(curr_time, 'start', 'day') % today 0:00:00

disp('tomorrow:');
dateshift(curr_time, 'end','day') % tomorrow 0:00:00

disp('beginning of the current hour');
dateshift(curr_time, 'start','hour')


%% conversions FROM datetime
disp('conversions FROM datetime...');
disp('current julian date:');
juliandate(datetime) 

disp('current posix date:');
posixtime(datetime) % unix time

disp('current datenum:');
datenum(datetime)
disp('current datevec:');
datevec(datetime) % get the [Y, M, D, h, m, s]

%% durations
disp('durations...')
disp(' now - beginning of day')
day_elapsed = curr_time - curr_day % a "duration"

disp('1.5 hours, as a duration')
some_time = hours(1.5)
disp(' ... in minutes')
minutes(some_time)
disp(' ... in seconds')
seconds(some_time)

disp('The time in 1.5 hours:')
curr_time + some_time

%% range of dates
disp(' RANGE of dates... (dividing a day into 6 hour increments)')
% increments default to 1 day!
dayrange= curr_day : hours(6) : curr_day+day(1)

disp(' RANGE of durations') 
mydurs = hours(0):hours(1):hours(3)
disp('    ... as minutes:')
minutes(mydurs)

%% plotting
x=mydurs;
y=rand(size(x));
figure('Name','plot vs durations')
plot(x,y,'o-'); % although small, x automatically labeld as 'hr'

x=dayrange
y=rand(size(x));
figure('Name','plot vs day')
plot(x,y,'o-'); % although small,labeled!

x=datetime(2015,1,1):years(.25):datetime(2017,10,1);
y=rand(size(x));
figure('Name','plot vs year')
plot(x,y,'o-'); % although small,labeled!

%% histogram
y=randn(1000,1) * years(3);
figure('Name','duration histogram')
histogram(y)

y=randn(1000,1) * years(3) + datetime(2010,1,1);
figure('Name','date histogram')
histogram(y)

