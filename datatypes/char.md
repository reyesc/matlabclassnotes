# datatype `char`

Ein `char` ("Zeichendatatyp") representiert Zeichen.

## Beispeile

To tell MATLAB that a value is a character, the value MUST be surrounded by single quotes. zB. `'a'`

```matlab
'a'          % ein Zeichen
'abc'        % ein Vektor von Zeichen
'$d3_$(Qbn'  % sie muss nicht A bis Z Buchstaben sein.
```

### Possible related errors

* `Undefined function or variable`
* `Error: The input character is not valid in MATLAB statements or expressions.`

Did you remember to wrap the value in single quotes? If not, then MATLAB tried to evaluate it, and couldn't figure out what it means.


## Extra infos

### Was meint mann, `char`

Zeichendatentyp.

### Was ist die unterschied zwischen `char` und `string`

a char treats each individual character as a value, together they form an array and must follow all the typical array rules.

```matlab
>> c = 'abcdefg'
>> length(c)
ans = 
   7
>> c(5)
ans = 
   'e'
```

a string treats each group of characters as a value

```matlab
>> s = "abcdefg"
>> length(s)
ans = 
   1
>> s(5) % error: Index exceeds matrix dimensions.
```
#### Translating between char and string

```matlab
>> 'abc'
ans = 
  'abc'
>> string(c)
ans =
   "abc"
>> char(s)
anse = 
   'abc'
```

### comparing char values
```matlab
>> 'hoi' == 'hoi' % vergelich jedes Buchstaben
ans =
   1x3 logical array
   1 1 1

>> 'hello' == 'hoi' % error: Matrix dimensions must agree.
```

Internally, the computer each character is represented by a number, so `'a'` does not equal `'A'`

```matlab
>> 'a' < 'z'  % wahr
>> 'a' < 'Z'  % falsch

>> double('azAZ') % überprüfen den Zahlenwert für jeden Buchstaben
ans =
    97   122    65    90
```

