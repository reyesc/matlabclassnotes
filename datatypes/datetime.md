# datatype `datetime`

_datetime_ represents a moment in time, such as right now, a year ago, or the moment you were born.  see the [datetime sample file](datetime_overview.m)

