# datatype `string`

Ein `string` ("Zeichenfolge-Datentyp") representiert Worte usw.

## Beispeile

To tell MATLAB that it is a character, it MUST be surrounded by double quotes. zB. `"a"`

```matlab
"a"
"abc" 
"nein oder ja"
```

### Possible related errors

* `Undefined function or variable`
* `Error: The input character is not valid in MATLAB statements or expressions.`

Did you remember to wrap the value in double quotes? If not, then MATLAB tried to evaluate it, and couldn't figure out what it means.


## Extra infos



### Was meint mann, `string`

Zeichenfolge-Datentyp. 

### Was ist die unterschied zwischen `char` und `string`

Es gibt dedizierte Funktionen, die machen `string` ein sehr nützliche Darstellung.  Mit STRING, mann kann:

```matlab
% werte vergleichen
>> "hello" == "hoi"  % falsch, aber gültig
ans = 
   logical
   0

>> wortA == wortA  % wahr
ans = 
   logical
   1
```

man can mit string und char vergleichen

```matlab
>> 'hoi' == "hoi" % wahr
ans = 
   logical
   1
```
