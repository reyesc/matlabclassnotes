# datatype `table`

A table organizes data as a table.

```matlab
rows = [1,1,1,2,2,2,3,3,3]  % 1xN double (numeric)
seats = 'abcabcabc'   % a 1xN char 
middle = [false true false false true false false true false] % 1xN logical

% Notice data above is all in rows. We want each to be a column (Nx1), so
% they will all be transposed (') when they are added
tb = table()  % create an empty table

tb.row = rows'
tb.seat = seats'
tb.middle = middle'
```

creates the following:
```
tb =

  9×3 table

    row    seat    middle
    ___    ____    ______

     1      a      false 
     1      b      true  
     1      c      false 
     2      a      false 
     2      b      true  
     2      c      false 
     3      a      false 
     3      b      true  
     3      c      false 

```

```
tb.seat  % get the 'seat' column

% is exactly the same as notice: {} not ()
tb.{:,'seat'}  % get all rows for the 'seat' column


% is exactly the same as
tb{:,2}  % get all rows for second column

% however...
tb(:,2) gives a sub-table

% example...
tb(tb.row == 2, :)  % get sub-table containing info for row #2
```


## Errors you WILL see

### a
```
>> tb{tb.row == 2}

Subscripting into a table using one subscript (as in t(i)) or three or more subscripts (as in
t(i,j,k)) is not supported. Always specify a row subscript and a variable subscript, as in
t(rows,vars).
```

*which means* you asked for specific rows from the table, but diidn't say which columns.

### b

```
>> tb{tb.row == 2, :}
Unable to concatenate the specified table variables.

Caused by:
    Error using horzcat
    The following error occurred converting from logical to char:
    Conversion to char from logical is not possible.
```

*which means* you useed the `{}` instead of `()`, so it removes the "table"is wrapper
from the data, trying to make a simple array.  But the table contains a bunch of
different data types, so it didn't like it

