# datatype `categorical` (categorisch)

Categories handle a finite number of values, and allow for more efficient processing and statistical queries.

Example of data that is good categorical data:
* Hurrikan-Kategorien  [1-5]
* Geschlecht ['m' oder 'f']
* Farben
* Magnitude Typen ['M', 'ML', 'MS', etc.]
