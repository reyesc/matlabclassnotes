# Using Sections
![section 0](resources/section_series/run_menu.png)


![section 0](resources/section_series/section_pre.png)

Klick  ![run and advance](resources/section_series/run_and_advance.png)

![section 1](resources/section_series/section_1.png)

Klick  ![run and advance](resources/section_series/run_and_advance.png)

![section 2](resources/section_series/section_2.png)

Klick  ![run and advance](resources/section_series/run_and_advance.png)

![section 3](resources/section_series/section_3.png)

Klick  ![run and advance](resources/section_series/run_and_advance.png)

![section 4](resources/section_series/section_4.png)